#!/usr/bin/env racket

;; This file is part of Matrix Racket SDK, a Matrix client-to-server library
;; Copyright (C) 2018  Aidan Gauland
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#lang racket

(require (combine-in
          (prefix-in mx: "client.rkt")
          (rename-in "client.rkt"
                     (mx-sync mx:sync))))
(require json)

(parameterize ([mx:server "example.net"]
               [mx:token (hash-ref (call-with-input-file* "/path/to/login-response.json" read-json) 'access_token)])
  (let* ([response (mx:sync)]
         [next-point (hash-ref (mx:response-body response) 'next_batch)])
    (pretty-display (mx:response-body response))))
