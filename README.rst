=================
Matrix Racket SDK
=================
Client-to-server library for the Matrix_ platform written in Racket_.

Nothing much to see here yet.  Tread carefully, and only if you are familiar with Racket and the `Matrix client-server API`_.

.. _Racket: https://racket-lang.org/
.. _Matrix: https://matrix.org/
.. _Matrix client-server API: https://matrix.org/docs/guides/client-server.html
